# BeagleBoard.org BeagleConnect Freedom

[![](graphics/BeagleConnect-Freedom-Hand.webp)](https://beagleconnect.org)

BeagleConnect™ Freedom is an open-hardware wireless hardware platform developed
by BeagleBoard.org and built around the TI CC1352P7 microcontroller, which
supports both 2.4-GHz and long-range, low-power Sub-1 GHz wireless protocols.
Rapidly prototyping of IoT applications is accelerated by hardware
compatibility with over 1,000 mikroBUS add-on sensors, acutators, indicators
and additional connectivity and storage options, and backed with software
support utilizing the Zephyr scalable and modular real-time operating system,
allowing developers to tailor the solution to their specific needs.
BeagleConnect Freedom further includes MSP430F5503 for USB-to-UART
functionality, temperature and humidity sensor, light sensor, SPI flash,
battery charger, buzzer, LEDs, and JTAG connections to make it a comprehensive
solution for IoT development and prototyping.

The TI CC1352P7 microcontroller (MCU) includes a 48-MHz Arm Cortex-M4F
processor, 704KB Flash memory, 256KB ROM, 8KB Cache SRAM, 144KB of ultra-low
leakage SRAM, and over-the-air upgrades (OTA) capability. This MCU provides
flexible support for many different protocols and bands making it suitable for
many different communication requirements.

## Board image

[![](graphics/BeagleConnect-Boards-Angled.webp)](https://beagleconnect.org)

## Pinout
[![](graphics/beagleconnect_freedom_annotated.webp)](https://beagleconnect.org)

## With case
[![](graphics/BeagleConnect-Freedom-with-case.webp)](https://beagleconnect.org)

## Links

* Terms and conditions: https://docs.beagleboard.org/latest/boards/terms-and-conditions
* Documentation: https://docs.beagleboard.org/latest/boards/beagleconnect/freedom
* Design: https://git.beagleboard.org/beagleconnect/freedom
* Support: https://forum.beagleboard.org/tag/bcf
* Repair: https://www.beagleboard.org/rma
* Purchase: https://beagleconnect.org

